
package de.hetzge.superreplace.extension;

import org.jboss.as.subsystem.test.AbstractSubsystemBaseTest;

import java.io.IOException;


public class SubsystemBaseParsingTestCase extends AbstractSubsystemBaseTest
{
	public SubsystemBaseParsingTestCase()
	{
		super( SubsystemExtension.SUBSYSTEM_NAME, new SubsystemExtension());
	}

	@Override
	protected String getSubsystemXml() throws IOException
	{
		return "<subsystem xmlns=\"" + SubsystemExtension.NAMESPACE + "\">" + "</subsystem>";
	}
}
