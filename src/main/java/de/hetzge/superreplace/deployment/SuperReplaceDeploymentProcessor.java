
package de.hetzge.superreplace.deployment;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.jboss.as.server.deployment.Attachments;
import org.jboss.as.server.deployment.DeploymentPhaseContext;
import org.jboss.as.server.deployment.DeploymentUnit;
import org.jboss.as.server.deployment.DeploymentUnitProcessingException;
import org.jboss.as.server.deployment.DeploymentUnitProcessor;
import org.jboss.as.server.deployment.DeploymentUtils;
import org.jboss.as.server.deployment.module.DelegatingClassFileTransformer;
import org.jboss.as.server.deployment.module.ResourceRoot;
import org.jboss.jandex.AnnotationInstance;
import org.jboss.jandex.ClassInfo;
import org.jboss.jandex.DotName;
import org.jboss.jandex.Index;
import org.jboss.logging.Logger;
import org.jboss.modules.Module;

import de.hetzge.superreplace.ClassFilterPredicate;
import de.hetzge.superreplace.Replace;
import de.hetzge.superreplace.Replacement;
import de.hetzge.superreplace.Utils;
import de.hetzge.superreplace.transform.ReplaceInstantiationClassFileTransformer;


/**
 * inspired by: ClassFileTransformerProcessor
 */
public class SuperReplaceDeploymentProcessor implements DeploymentUnitProcessor
{
	private static final Logger LOGGER = Logger.getLogger( SuperReplaceDeploymentProcessor.class);

	@Override
	public void deploy(DeploymentPhaseContext phaseContext) throws DeploymentUnitProcessingException
	{
		final DeploymentUnit deploymentUnit = phaseContext.getDeploymentUnit();

		LOGGER.info( String.format( "start super-replace deployment processor for unit '%s'", deploymentUnit.getName()));

		for( ResourceRoot resourceRoot : DeploymentUtils.allResourceRoots( deploymentUnit))
		{
			final String rootName = resourceRoot.getRootName();
			final Index index = resourceRoot.getAttachment( Attachments.ANNOTATION_INDEX);
			final Module module = deploymentUnit.getAttachment( Attachments.MODULE);
			final DelegatingClassFileTransformer transformer = deploymentUnit.getAttachment( DelegatingClassFileTransformer.ATTACHMENT_KEY);

			LOGGER.info( String.format( "super-replace resource root '%s' (index: %s, module: %s, transformer: %s): ", rootName,
				index != null, module != null, transformer != null));

			if( index != null && module != null && transformer != null)
			{
				final List<Replacement> replacements = index.getAnnotations( DotName.createSimple( Replace.class.getName())).stream()
					.map( AnnotationInstance::target).filter( ClassInfo.class::isInstance).map( ClassInfo.class::cast)
					.map( classInfo -> clazz( classInfo, module)).map( Replacement::new).collect( Collectors.toList());

				Utils.logMissingConstructors( replacements);
				Utils.logConflictingReplacements( replacements);
				Utils.logReplacementConfigs( replacements);

				if( !replacements.isEmpty())
				{
					transformer.addTransformer( new ReplaceInstantiationClassFileTransformer( replacements,
						new ClassFilterPredicate.Builder().withIncludePackageNames( Arrays.asList( "mc")).build()));

					LOGGER.info( "super-replace transformer added");
				}
			}
		}
	}

	@Override
	public void undeploy(DeploymentUnit context)
	{
		// do nothing here
	}

	private Class<?> clazz(ClassInfo classInfo, Module module)
	{
		try
		{
			return Class.forName( classInfo.name().toString(), false, module.getClassLoader());
		}
		catch( ClassNotFoundException e)
		{
			throw new IllegalStateException( e);
		}
	}
}
