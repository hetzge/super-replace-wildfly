
package de.hetzge.superreplace.extension;

import org.jboss.as.controller.AbstractRemoveStepHandler;
import org.jboss.as.controller.OperationContext;
import org.jboss.as.controller.OperationFailedException;
import org.jboss.dmr.ModelNode;


class SubsystemRemove extends AbstractRemoveStepHandler
{
	static final SubsystemRemove INSTANCE = new SubsystemRemove();

	private SubsystemRemove()
	{
	}

	@Override
	protected void performRuntime(OperationContext context, ModelNode operation, ModelNode model) throws OperationFailedException
	{
		// Remove any services installed by the corresponding add handler here
		// context.removeService(ServiceName.of("some", "name"));
	}
}
