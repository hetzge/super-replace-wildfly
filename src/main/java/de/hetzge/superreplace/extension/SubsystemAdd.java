
package de.hetzge.superreplace.extension;

import java.util.List;

import org.jboss.as.controller.AbstractBoottimeAddStepHandler;
import org.jboss.as.controller.OperationContext;
import org.jboss.as.controller.OperationFailedException;
import org.jboss.as.controller.ServiceVerificationHandler;
import org.jboss.as.server.AbstractDeploymentChainStep;
import org.jboss.as.server.DeploymentProcessorTarget;
import org.jboss.as.server.deployment.Phase;
import org.jboss.dmr.ModelNode;
import org.jboss.msc.service.ServiceController;

import de.hetzge.superreplace.deployment.SuperReplaceDeploymentProcessor;


class SubsystemAdd extends AbstractBoottimeAddStepHandler
{
	static final SubsystemAdd INSTANCE = new SubsystemAdd();

	private SubsystemAdd()
	{
	}

	@Override
	protected void populateModel(ModelNode operation, ModelNode model) throws OperationFailedException
	{
	}

	@Override
	public void performBoottime(OperationContext context, ModelNode operation, ModelNode model,
		ServiceVerificationHandler verificationHandler, List<ServiceController<?>> newControllers) throws OperationFailedException
	{
		context.addStep( new AbstractDeploymentChainStep()
		{
			public void execute(DeploymentProcessorTarget processorTarget)
			{
				processorTarget.addDeploymentProcessor( SubsystemExtension.SUBSYSTEM_NAME, Phase.POST_MODULE,
					Phase.POST_MODULE_REFLECTION_INDEX + 1, new SuperReplaceDeploymentProcessor());

			}
		}, OperationContext.Stage.RUNTIME);
	}
}
